
" Neovim > v0.8.1

"============================================================
" Plugins
"============================================================

call plug#begin()

" File system explorer for the Vim editor. Using this plugin, users can
" visually browse complex directory hierarchies, quickly open files for
" reading or editing, and perform basic file system operations.
  Plug 'scrooloose/nerdtree'

" Insert or delete brackets, parens, quotes in pair.
  Plug 'jiangmiao/auto-pairs'

" A simple, easy-to-use Vim alignment plugin.
  Plug 'junegunn/vim-easy-align'

" Shows a git diff in the sign column. It shows which lines have been added,
" modified, or removed. You can also preview, stage, and undo individual
" hunks; and stage partial hunks. The plugin also provides a hunk text
" object.
  Plug 'airblade/vim-gitgutter'

" Highly extendable fuzzy finder over lists. Built on the latest awesome
" features from neovim core. Telescope is centered around modularity,
" allowing for easy customization.
  Plug 'nvim-telescope/telescope.nvim'
  Plug 'nvim-lua/plenary.nvim'

" Language completion CoC.nvim (Conquer Of Completion)
  Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()


"============================================================
" General
"============================================================

" Editing
set tabstop=4             " The width of a TAB is set to 2.
set shiftwidth=4          " Indents will have a width of 2.
set softtabstop=4         " Sets the number of columns for a TAB.
set expandtab             " Expand TABs to spaces.
set nowrap                " do not automatically wrap on load
set formatoptions-=t      " do not automatically wrap text when typing
set number                " Sets linenumbers
set noequalalways         " This will cause vim to size each new split relative the current split
set nolist                " Hide invisible characters
set mouse+=a              " Add mouse support
set autoindent            " Useful for structured text files
set laststatus=1          " Disable status bar at the bottom
set spelllang=en          " Set spelling language
set clipboard=unnamedplus " Make normal yanks to use the system clipboard
set ignorecase            " When searching ignore case sensitivity
set smartcase             " Enable case sensitivity only when using uppercase characters
set splitright            " When splitting, open on right side
"set relativenumber       " Sets relative linenumbers

" Theme
colorscheme lunaperche

" Cosmetics
highlight LineNr     ctermfg=darkgrey
highlight Directory  ctermfg=111
highlight StatusLine ctermfg=0


"============================================================
" Telescope
"============================================================

" This is the default option:
"   - Preview window on the right with 50% width
"   - CTRL-/ will toggle preview window.
" - Note that this array is passed as arguments to fzf#vim#with_preview function.
" - To learn more about preview window options, see `--preview-window` section of `man fzf`.
let g:fzf_preview_window = ['right:50%', 'ctrl-/']

" Preview window on the upper side of the window with 40% height,
" hidden by default, ctrl-/ to toggle
let g:fzf_preview_window = ['up:40%:hidden', 'ctrl-/']

" Empty value to disable preview window altogether
let g:fzf_preview_window = []


"============================================================
" COC is an intellisense engine for Vim/Neovim.
"============================================================

" Set local node binary
let g:coc_node_path = '~/.local/bin/node'

" Give more space for displaying messages.
set cmdheight=1

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" if has('patch8.1.1068')
"   " Use `complete_info` if your (Neo)Vim version supports it.
"   inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
" else
"   imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>


"============================================================
" Functions
"============================================================

function! ToggleTextWrap()
  if &textwidth == 0
    set formatoptions+=t
    set textwidth=79
    set colorcolumn=80
  else
    set formatoptions-=t
    set textwidth=0
    set colorcolumn=0
  endif
endfunction

function! ToggleSpellCheck()
  set spell!
  if &spell
    echo "Spellcheck ON"
  else
    echo "Spellcheck OFF"
  endif
endfunction

function! ToggleNerdTree()
  set splitbelow
  :let g:NERDTreeMinimalUI = 1
  :let g:NERDTreeDirArrows = 1
  :let g:NERDTreeWinSizer=100
  :let g:NERDTreeWinPos="left"
  :let g:NERDTreeNaturalSort=1
  :NERDTreeToggle
  :if isdirectory('.git')
    :SetGitRoot
  :else
    :set autochdir
  :endif
endfunction

function! FuzzyFind()
  :cd `git rev-parse --show-toplevel`
  :wincmd l
  Telescope git_files 
endfunction

function! SetGitRoot()
  set noautochdir
  :cd %:p:h
  :cd `git rev-parse --show-toplevel`
  :NERDTree .
endfunction


"============================================================
" Bindings
"============================================================
"
" Custom commands
command ToggleNerdTree   :call ToggleNerdTree()
command FuzzyFind        :call FuzzyFind()
command SetGitRoot       :call SetGitRoot()
command ToggleTextWrap   :call ToggleTextWrap()
command ToggleSpellCheck :call ToggleSpellCheck()

" Navigate
nnoremap <A-j> <C-d>
nnoremap <A-k> <C-u>

" File navigation
nnoremap <C-e> :ToggleNerdTree<enter>
nnoremap <C-f> :FuzzyFind<enter>

" Tab management
nnoremap tn :tabnew<enter>
nnoremap tc :tabclose<enter>
nnoremap <C-m> :tabnext<enter>
nnoremap <C-n> :tabprevious<enter>

" Window management
nnoremap \| :vsplit .<enter>
nnoremap <  :vertical resize -10<enter>
nnoremap >  :vertical resize +10<enter>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Text Aligning
vnoremap af :EasyAlign 1/[:;\|=,]/<enter>  " Align first
vnoremap aa :EasyAlign */[:;\|=,]/<enter>  " Align all
vnoremap as :EasyAlign */[\ ]/<enter>      " Align spaces
vnoremap ap :EasyAlign */[\|]/<enter>      " Align pipes
vnoremap aj :!jq .<enter>                  " Align JSON

" Other
imap     fj <Esc>
imap     fk <Esc>

nnoremap ff za
nnoremap <A-q> :qa!<enter>
nnoremap <A-s> :ToggleSpellCheck<enter>
nnoremap <A-t> :ToggleTextWrap<enter>
nnoremap <C-s> :w<enter>

" Hide root path name
augroup nerdtreehidecwd
  autocmd!
  autocmd FileType nerdtree syntax match NERDTreeHideCWD #^[</].*$# conceal
augroup end


"============================================================
" Git Commands
"============================================================

function! GitBranch()
  return system("git branch 2> /dev/null | grep '*' | cut -c3- | tr -d '\n'")
endfunction

function! GitStatusModified()
  return system("git status 2> /dev/null | grep ': ' | wc -l | tr -d '\n'")
endfunction

function! GitBranch()
  return system("git branch 2> /dev/null | grep '*' | cut -c3- | tr -d '\n'")
endfunction

function! GitStatusModified()
  return system("git status 2> /dev/null | grep ': ' | wc -l | tr -d '\n'")
endfunction

nnoremap gd :!git diff<enter>
nnoremap gs :!git status<enter>
nnoremap ga :!git add 
nnoremap gc :!git commit -m '
nnoremap gC :!git citool<enter>
nnoremap gG :!git gui<enter>
nnoremap gu :!git pull --verbose<enter>
nnoremap gp :!git push --verbose<enter>
nnoremap gb :!git branch<enter>
nnoremap gB :!git branch<space>
nnoremap gD :!git branch --delete --force<space>
nnoremap go :!git switch<space>
nnoremap gm :!git merge --squash<space>
nnoremap gl :!git log --reverse<enter>
nnoremap gi :!git init<enter>
nnoremap gh :GitGutterLineHighlightsToggle<enter>
nnoremap gt :SetGitRoot<enter>

set statusline=
set statusline+=\ Branch:%{GitBranch()}
set statusline+=\ Modified:%{GitStatusModified()}
set statusline+=\ %f


"============================================================
" File handler
"============================================================

autocmd BufRead *.adoc normal zM
autocmd BufRead *.asciidoc normal zM
autocmd BufEnter *.png,*.jpg,*.gif exec "silent ! feh ".expand("%")
autocmd BufEnter *.png,*.jpg,*.gif :b#
autocmd BufEnter *.pdf exec "silent ! mupdf ".expand("%") 
autocmd BufEnter *.pdf :b#
autocmd BufEnter *.sc exec "silent ! konsole -e sc ".expand("%") 
autocmd BufEnter *.sc :b#


"============================================================
" EOF
"============================================================

