#!/bin/bash

######################################
# Define versions
######################################

neovim_dl='https://github.com/neovim/neovim/releases/download/v0.8.1/nvim-linux64.tar.gz'
nodejs_dl='https://nodejs.org/dist/v18.12.1/node-v18.12.1-linux-x64.tar.gz'

######################################
# Download and unpack
######################################

neovim_ver=`echo ${neovim_dl} | awk -F'/' '{print $8}'`
nodejs_ver=`echo ${nodejs_dl} | awk -F'/' '{print $5}'`

cwd=`pwd`
mkdir -vp ~/.local/bin
mkdir -vp ~/.local/share/

cd ~/.local/share || exit 1

echo "neovim-version: ${neovim_ver}"
echo "node-version: ${nodejs_ver}"

yesno="y"
if [[ $1 != "-y" ]]; then
  read -p "Download and install Neovim and Nodejs in ~/.local/share ? (Y/n): " yesno
fi

if [[ -n `echo ${yesno} | grep -i y` || -z ${yesno} ]]; then

  ######################################
  # (Re)Install Node
  ######################################

  rm -rf node-*
  wget ${nodejs_dl} || exit 1
  echo "Unpacking node-${nodejs_ver}-linux-x64.tar.gz..."
  tar xf node-${nodejs_ver}-linux-x64.tar.gz
  rm -v node-${nodejs_ver}-linux-x64.tar.gz
  mv -v node-${nodejs_ver}-linux-x64 node-${nodejs_ver}

  ######################################
  # (Re)Install Neovim
  ######################################

  rm -rf nvim-*
  wget ${neovim_dl} || exit 1
  echo "Unpacking nvim-linux-x64.tar.gz..."
  tar xzf nvim-linux64.tar.gz
  rm -v nvim-linux64.tar.gz
  mv -v nvim-linux64 nvim-${neovim_ver}

  ######################################
  # (Re)Install Vim Plugin
  ######################################

  mkdir -vp ~/.local/share/nvim/site/autoload
  cd ~/.local/share/nvim/site/autoload
  if [[ ! -f plug.vim ]]
  then
    wget https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim || exit 1
  fi

  ######################################
  # (Re)Install Symlinks
  ######################################

  cd ~/.local/bin
  rm nvim node npx npm
  ln -s ~/.local/share/nvim-${neovim_ver}/bin/nvim 
  ln -s ~/.local/share/node-${nodejs_ver}/bin/node
  ln -s ~/.local/share/node-${nodejs_ver}/bin/npx
  ln -s ~/.local/share/node-${nodejs_ver}/bin/npm
fi

######################################
# Configure Neovim & VIM
######################################

yesno="y"
if [[ $1 != "-y" ]]; then
  read -p "Setup and use Neovim and VIM configs from this repository ? (Y/n): " yesno
fi

if [[ -n `echo ${yesno} | grep -i y` || -z ${yesno} ]]; then

  cd ${cwd}

  mkdir -vp ~/.config/nvim
  mkdir -vp ~/.vim
  
  cp -vi ./neovim/init.vim ~/.config/nvim/init.vim
  cp -vi ./vim/vimrc ~/.vim/vimrc
  cp -vi ./tmux/tmux.conf ~/.tmux.conf

fi

exit


