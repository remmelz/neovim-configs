
# Tmux

tmux is a terminal multiplexer for Unix-like operating systems. It allows
multiple terminal sessions to be accessed simultaneously in a single window. It
is useful for running more than one command-line program at the same time. It
can also be used to detach processes from their controlling terminals, allowing
SSH sessions to remain active without being visible.

Add the following to the ~/.bashrc file to check for running Tmux sessions:

    if [[ -f ~/.tmux.conf ]]; then
      if [[ `ps ux | grep 'tmux$' | wc -l` -gt 0 ]]; then
        printf 'tmux sessions: '
        tmux ls 2> /dev/null
      fi
    fi


