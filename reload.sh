#!/bin/bash

while true; do

  # Wait for changes...
  inotifywait -q -r -e modify --exclude swp .

  # Execute some command(s)...

  date
  echo "Hello World!"

done
